// Dependencies
const chaiHttp = require('chai-http');
const chai = require('chai');

// Assets
const SERVER = require('../src/server');

// Assertions
chai.should();
chai.use(chaiHttp);

// eslint-disable-next-line no-undef
describe('Ediciones nuevas', () => {
  // eslint-disable-next-line no-undef
  it('Debe de crearse un articulo nuevo en un numero en la gaceta ordinaria', (done) => {
    chai
      .request(SERVER)
      .post('/article')
      .type('form')
      .send({
        fecha: '15/02/2021',
        edicion: 'ordinaria',
        numero: 1633,
        titulo: 'Lorem Ipsum',
        contenido:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus non leo nec leo cursus laoreet. Quisque'
            + ' sit amet metus vehicula, malesuada lacus eget, dapibus nulla. Nulla facilisi. Cras vel nunc maximus,'
            + ' suscipit metus malesuada, tincidunt leo. Duis ut volutpat sapien. Praesent feugiat semper tortor, quis'
            + ' consectetur urna venenatis at. Nunc at tristique elit. Aliquam maximus efficitur tortor a congue. '
            + 'Morbiid nunc eu tellus pulvinar imperdiet. Mauris dolor velit, venenatis in elit a, blandit '
            + 'porta tortor. Integer interdum ex molestie, egestas nisl vel, hendrerit lectus. Vestibulum nec'
            + ' dolor justo.\n\n'
          + 'Duis semper pharetra purus sit amet porttitor. Sed ullamcorper commodo sollicitudin. Sed consequat'
            + ' augue quis eros convallis venenatis. Sed leo sem, viverra at magna sit amet, congue condimentum'
            + ' libero. Sed efficitur maximus congue. Nullam et maximus eros. Vivamus tristique tincidunt bibendum.'
            + ' Sed pulvinar dui eu urna tincidunt feugiat.\n\n'
          + 'Nullam commodo laoreet suscipit. Pellentesque malesuada, metus ullamcorper interdum rutrum, nulla'
            + ' tellus pharetra nunc, sit amet rhoncus neque tortor et neque. Aliquam sapien risus, ornare vel'
            + ' euismod ut, lacinia sit amet nisl. Maecenas vel mattis lorem. Nunc eget ante nisl. Donec scelerisque'
            + ' urna sit amet aliquam egestas. Nam placerat ligula ac augue eleifend sollicitudin. Duis ornare, magna'
            + ' sed tempor interdum, libero dui dignissim mauris, non pretium ligula sem id felis.',
      })
      .end((err, res) => {
        res.status.should.equal(201);
        res.type.should.equal('application/json');
        res.body.message.should.eql(
          'Se ha creado el articulo "Lorem Ipsum" a la edición ordinaria (no. 1633) con éxito!',
        );
        done();
      });
  });

  // eslint-disable-next-line no-undef
  it('Debe de dar error por cargar artículos ya existentes sin modificar', (done) => {
    chai
      .request(SERVER)
      .post('/article')
      .type('form')
      .send({
        fecha: '15/02/2021',
        edicion: 'ordinaria',
        numero: 1633,
        titulo: 'Lorem Ipsum',
        contenido:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus non leo nec leo cursus laoreet. Quisque'
            + ' sit amet metus vehicula, malesuada lacus eget, dapibus nulla. Nulla facilisi. Cras vel nunc maximus,'
            + ' suscipit metus malesuada, tincidunt leo. Duis ut volutpat sapien. Praesent feugiat semper tortor, quis'
            + ' consectetur urna venenatis at. Nunc at tristique elit. Aliquam maximus efficitur tortor a congue. '
            + 'Morbiid nunc eu tellus pulvinar imperdiet. Mauris dolor velit, venenatis in elit a, blandit '
            + 'porta tortor. Integer interdum ex molestie, egestas nisl vel, hendrerit lectus. Vestibulum nec'
            + ' dolor justo.\n'
            + 'Duis semper pharetra purus sit amet porttitor. Sed ullamcorper commodo sollicitudin. Sed consequat'
            + ' augue quis eros convallis venenatis. Sed leo sem, viverra at magna sit amet, congue condimentum'
            + ' libero. Sed efficitur maximus congue. Nullam et maximus eros. Vivamus tristique tincidunt bibendum.'
            + ' Sed pulvinar dui eu urna tincidunt feugiat.\n'
            + 'Nullam commodo laoreet suscipit. Pellentesque malesuada, metus ullamcorper interdum rutrum, nulla'
            + ' tellus pharetra nunc, sit amet rhoncus neque tortor et neque. Aliquam sapien risus, ornare vel'
            + ' euismod ut, lacinia sit amet nisl. Maecenas vel mattis lorem. Nunc eget ante nisl. Donec scelerisque'
            + ' urna sit amet aliquam egestas. Nam placerat ligula ac augue eleifend sollicitudin. Duis ornare, magna'
            + ' sed tempor interdum, libero dui dignissim mauris, non pretium ligula sem id felis.',
      })
      .end((err, res) => {
        res.status.should.equal(409);
        res.type.should.equal('application/json');
        res.body.message.should.eql(
          'El articulo "Lorem Ipsum" que está en la edición ordinaria (no. 1633) ya existe!',
        );
        done();
      });
  });

  // eslint-disable-next-line no-undef
  it('Debe de modificar un articulo ya existente', (done) => {
    chai
      .request(SERVER)
      .patch('/article')
      .type('form')
      .send({
        edicionAnterior: 'ordinaria',
        numeroAnterior: 1633,
        tituloAnterior: 'Lorem Ipsum',
        fecha: '15/02/2021',
        edicion: 'extraordinaria',
        numero: 350,
        titulo: 'Nam Mattis',
        contenido:
            'Nam mattis consectetur suscipit. Praesent molestie sed orci sed finibus. Curabitur eleifend lacus nec'
            + ' placerat euismod. Ut vitae libero eros. Proin in nisi id eros tempus fringilla vel vitae velit.'
            + ' Vestibulum facilisis, dui non venenatis placerat, mi magna placerat nulla, ut tincidunt erat'
            + ' nulla eleifend dolor. Vivamus vitae mauris at metus commodo gravida nec sed nisi. Proin elementum'
            + ' volutpat imperdiet. Donec dictum est leo, tincidunt hendrerit elit auctor ac. Suspendisse lacinia metus'
            + ' sed nulla vulputate rutrum. In in orci nisi.'
            + '\nFusce interdum facilisis molestie. Nunc elit lorem, ornare non ante ac, porta viverra massa.'
            + ' Morbi semper pulvinar nulla quis fringilla. Sed euismod tristique posuere. Mauris arcu erat, rutrum'
            + ' sit amet dolor eget, consectetur ultrices justo. Fusce molestie eu tellus ultrices varius.'
            + ' Vestibulum volutpat odio ipsum. Aliquam venenatis arcu in finibus scelerisque. '
            + 'Pellentesque mauris augue, pharetra eu ultrices a, luctus sed sem. Phasellus vel interdum libero. '
            + 'Curabitur vitae auctor turpis. Vivamus quis augue justo. In blandit finibus lacus. Pellentesque quis '
            + 'libero ac urna ullamcorper molestie a facilisis orci. Nulla blandit rhoncus augue, faucibus sodales '
            + 'velit auctor non.',
      })
      .end((err, res) => {
        res.status.should.equal(202);
        res.type.should.equal('application/json');
        res.body.message.should.eql(
          'Los cambios al articulo "Lorem Ipsum" a la edición ordinaria (no. 1633) se aplicaron con éxito!',
        );
        done();
      });
  });
});

// eslint-disable-next-line no-undef
describe('Lectura de ediciones', () => {
  // eslint-disable-next-line no-undef
  it('Debe de traer un artículo que solicité', (done) => {
    chai
      .request(SERVER)
      .get('/extraordinaria/350')
      .send({ titulo: 'Nam Mattis' })
      .end((err, res) => {
        res.status.should.equal(200);
        res.type.should.equal('application/json');
        res.body.titulo.should.eql('Nam Mattis');
        res.body.numero.should.equal(350);
        res.body.edicion.should.eql('extraordinaria');
        res.body.fecha.should.eql('15/02/2021');
        done();
      });
  });

  // eslint-disable-next-line no-undef
  it('Debe de dar error al tratar de solicitar un articulo que no existe', (done) => {
    chai
      .request(SERVER)
      .get('/ordinaria/1633')
      .send({ titulo: 'Dolor Sit Amet' })
      .end((err, res) => {
        res.status.should.equal(404);
        res.type.should.equal('application/json');
        res.body.message.should.eql('El artículo "Dolor Sit Amet" no existe en la edición ordinaria no. 1633!');
        done();
      });
  });

  // eslint-disable-next-line no-undef
  it('Debe de darme todos los indices que están en el numero', (done) => {
    chai
      .request(SERVER)
      .get('/index')
      .send({
        edicion: 'extraordinaria',
        numero: 350,
      })
      .end((err, res) => {
        res.status.should.equal(200);
        res.type.should.equal('application/json');
        res.body.fecha.should.eql('15/02/2021');
        res.body.edicion.should.eql('extraordinaria');
        done();
      });
  });
});
