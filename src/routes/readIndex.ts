import Router from 'koa-router';
import ReadData from '../controller/ReadData';

const ROUTER: Router = new Router();

ROUTER.get('/index', async (ctx: any) => {
  const DATOS = ctx.request.body;

  [ctx.status, ctx.body] = await new ReadData(DATOS, false).run();
});

export default ROUTER;
