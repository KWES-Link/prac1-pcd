import Router from 'koa-router';
import ReadData from '../controller/ReadData';

const ROUTER: Router = new Router();

ROUTER.get('/:edicion/:numero', async (ctx: any) => {
  const TITULO: string = ctx.request.body.titulo;
  const EDICION: string = ctx.request.url.split('/')[1];
  const NUMERO: number = ctx.request.url.split('/')[2];

  [ctx.status, ctx.body] = await new ReadData({
    titulo: TITULO,
    edicion: EDICION,
    numero: NUMERO,
  }, true).run();
});

export default ROUTER;
