"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const koa_router_1 = __importDefault(require("koa-router"));
const ReadData_1 = __importDefault(require("../controller/ReadData"));
const ROUTER = new koa_router_1.default();
ROUTER.get('/index', async (ctx) => {
    const DATOS = ctx.request.body;
    [ctx.status, ctx.body] = await new ReadData_1.default(DATOS, false).run();
});
exports.default = ROUTER;
//# sourceMappingURL=readIndex.js.map