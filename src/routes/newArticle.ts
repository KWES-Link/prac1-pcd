import Router from 'koa-router';
import SaveData from '../controller/SaveData';

const ROUTER: Router = new Router();

ROUTER.post('/article', async (ctx: any) => {
  const DATOS: any = ctx.request.body;

  /**
   * Valida si los campos contienen valores
   */

  if (!DATOS.fecha || !DATOS.edicion || !DATOS.numero || !DATOS.titulo || !DATOS.contenido) {
    // eslint-disable-next-line no-nested-ternary
    const CAMPO: string = (!DATOS.fecha) ? 'fecha'
    // eslint-disable-next-line no-nested-ternary
      : ((!DATOS.edicion) ? 'edición'
        : ((!DATOS.numero) ? 'numero' : 'contenido'));
    ctx.status = 400;
    ctx.body = {
      status: 'Invalid content',
      message: `El campo ${CAMPO} no puede ir vacío!`,
    };
  } else {
    [ctx.status, ctx.body] = await new SaveData(DATOS, true).run();
  }
});

export default ROUTER;
