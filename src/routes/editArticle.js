"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const koa_router_1 = __importDefault(require("koa-router"));
const SaveData_1 = __importDefault(require("../controller/SaveData"));
const ROUTER = new koa_router_1.default();
ROUTER.patch('/article', async (ctx) => {
    const DATOS = ctx.request.body;
    /**
       * Valida si los campos contienen valores
       */
    if (!DATOS.fecha || !DATOS.edicion || !DATOS.numero || !DATOS.titulo || !DATOS.contenido) {
        // eslint-disable-next-line no-nested-ternary
        const CAMPO = (!DATOS.fecha) ? 'fecha'
            // eslint-disable-next-line no-nested-ternary
            : ((!DATOS.edicion) ? 'edición'
                : ((!DATOS.numero) ? 'numero' : 'contenido'));
        ctx.status = 400;
        ctx.body = {
            status: 'Invalid content',
            message: `El campo ${CAMPO} no puede ir vacío!`,
        };
    }
    else {
        [ctx.status, ctx.body] = await new SaveData_1.default(DATOS, false).run();
    }
});
exports.default = ROUTER;
//# sourceMappingURL=editArticle.js.map