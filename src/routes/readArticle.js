"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const koa_router_1 = __importDefault(require("koa-router"));
const ReadData_1 = __importDefault(require("../controller/ReadData"));
const ROUTER = new koa_router_1.default();
ROUTER.get('/:edicion/:numero', async (ctx) => {
    const TITULO = ctx.request.body.titulo;
    const EDICION = ctx.request.url.split('/')[1];
    const NUMERO = ctx.request.url.split('/')[2];
    [ctx.status, ctx.body] = await new ReadData_1.default({
        titulo: TITULO,
        edicion: EDICION,
        numero: NUMERO,
    }, true).run();
});
exports.default = ROUTER;
//# sourceMappingURL=readArticle.js.map