"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// Dependencies
const koa_1 = __importDefault(require("koa"));
const koa_bodyparser_1 = __importDefault(require("koa-bodyparser"));
const koa2_cors_1 = __importDefault(require("koa2-cors"));
const koa_logger_1 = __importDefault(require("koa-logger"));
// Assets
const newArticle_1 = __importDefault(require("./routes/newArticle"));
const editArticle_1 = __importDefault(require("./routes/editArticle"));
const readArticle_1 = __importDefault(require("./routes/readArticle"));
const readIndex_1 = __importDefault(require("./routes/readIndex"));
const APP = new koa_1.default();
const PORT = process.env.PORT || 3000;
// MIDDLEWARES
APP.use(koa_bodyparser_1.default()).use(koa2_cors_1.default({ origin: '*' }))
    .use(koa_logger_1.default())
    .use(newArticle_1.default.routes())
    .use(editArticle_1.default.routes())
    .use(readArticle_1.default.routes())
    .use(readIndex_1.default.routes());
// Creación del servidor
const SERVER = APP.listen(PORT, async () => console.log(`El servidor está en el puerto ${PORT}`))
    .on('error', (err) => console.error(err));
module.exports = SERVER;
//# sourceMappingURL=server.js.map