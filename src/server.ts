// Dependencies
import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import cors from 'koa2-cors';
import logger from 'koa-logger';

// Assets
import newArticle from './routes/newArticle';
import editArticle from './routes/editArticle';
import readArticle from './routes/readArticle';
import readIndex from './routes/readIndex';

const APP: Koa = new Koa();
const PORT: string | number = process.env.PORT || 3000;

// MIDDLEWARES
APP.use(bodyParser()).use(cors({ origin: '*' }))
  .use(logger())
  .use(newArticle.routes())
  .use(editArticle.routes())
  .use(readArticle.routes())
  .use(readIndex.routes());

// Creación del servidor
const SERVER: Koa = APP.listen(PORT, async () => console.log(`El servidor está en el puerto ${PORT}`))
  .on('error', (err) => console.error(err));

module.exports = SERVER;
