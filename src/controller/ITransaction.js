"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const GacetaRepository_1 = __importDefault(require("../repositories/GacetaRepository"));
class ITransaction {
    constructor(datos, newData) {
        this.repo = new GacetaRepository_1.default(datos);
        this.newData = newData;
    }
    /**
     * Dependiendo de la bandera de datos nuevos (newData) va a ser el metodo que se va a
     * invocar para que guarde un artículo nuevo o modifique el artículo existente.
     * Si sale exitoso va a devolver true para que de el mensaje respectivo.
     * En caso de haber error va a devolver false
     */
    async ISave() {
        try {
            return !!(await (this.newData ? this.repo.setArticulo() : this.repo.setCambioArticulo()));
        }
        catch (err) {
            return false;
        }
    }
    /**
     * funciona de manera similar que en la interfaz de guardado; En caso de que haya error
     * devuelve error 500 con su mensaje respectivo.
     * En caso de que salga bien, devuelve su respectivo código de respuesta con su mensaje.
     * La diferencia en este metodo es que en este caso se ocupa una bandera que está
     * en el parámetro de esta interfaz la cual se explica a continuación:
     *
     * @param getSingle indica si va a obtener solo un artículo (true) o los indices (false)
     */
    async IRead(getSingle) {
        try {
            let salida;
            if (getSingle)
                salida = await this.repo.getArticulo();
            else
                salida = await this.repo.getArticulos();
            return salida;
        }
        catch (err) {
            return [500, {
                    status: 'Transaction Error',
                    message: 'Ha ocurrido un grave error al ejecutar la transacción!',
                }];
        }
    }
}
exports.default = ITransaction;
//# sourceMappingURL=ITransaction.js.map