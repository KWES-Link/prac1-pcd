import ITransaction from './ITransaction';

export default class SaveData {
  datos: any;

  operacion: ITransaction;

  postFlag: boolean;

  /**
   * Recibe la información para que esta sea procesada antes de guardar en la base de datos
   * @param datos Recibe la información que se va a escribir a la base de datos
   * @param postFlag Sirve para indicar si se esta llamando desde un post o patch
   */
  constructor(datos: object, postFlag: boolean) {
    this.datos = datos;
    this.postFlag = postFlag;
    this.operacion = new ITransaction(this.datos, this.postFlag);
  }

  async run(): Promise<[number, object]> {
    if (this.postFlag) {
      const estado: [number, object] = await this.esDuplicado();
      if (estado[0] === 200) {
        return [
          409,
          {
            status: 'Content already exists',
            message:
              `El articulo "${this.datos.titulo}" que está en la edición ${this.datos.edicion}`
              + ` (no. ${this.datos.numero}) ya existe!`,
          },
        ];
      }
      if (estado[0] === 500) return estado;
    } else if (!this.postFlag && (await this.esModificacionClonada())) {
      return [
        409,
        {
          status: 'Content modified you tried to save already exist',
          message:
            `El articulo "${this.datos.titulo}" que está en la edición ${this.datos.edicion}`
            + `(no. ${this.datos.numero}) que solía llamarse "${this.datos.tituloAnterior}"`
            + `(ed. ${this.datos.edicion} no. ${this.datos.numeroAnterior}) y que estas tratado de guardar,`
            + 'se está duplicando. Revisa tu información e intentalo de nuevo!',
        },
      ];
    }
    if (await this.operacion.ISave()) {
      const MENSAJE: string = this.postFlag
        ? `Se ha creado el articulo "${this.datos.titulo}" a la edición `
          + `${this.datos.edicion} (no. ${this.datos.numero}) con éxito!`
        : `Los cambios al articulo "${this.datos.tituloAnterior}" a la edición ${this.datos.edicionAnterior} `
          + `(no. ${this.datos.numeroAnterior}) se aplicaron con éxito!`;

      return [
        this.postFlag ? 201 : 202,
        {
          status: 'Success',
          message: MENSAJE,
        },
      ];
    }
    return [
      500,
      {
        status: 'Transaction Error',
        message: 'Ha ocurrido un grave error al ejecutar la transacción!',
      },
    ];
  }

  /**
   * Este método busca el articulo que se desea agregar, esto para evitar duplicados.
   */
  async esDuplicado(): Promise<[number, object]> {
    // eslint-disable-next-line no-return-await
    return (await this.operacion.IRead(true));
  }

  /**
   * Este método busca el articulo que se desea insertar como modificación, si ya existe,
   * devuelve true.
   * Esto para evitar duplicados.
   */
  async esModificacionClonada(): Promise<boolean> {
    const codigo: [number, object] = await (await this.operacion.IRead(true));
    if (codigo[0] === 200) {
      const content: [number, any] = await (await this.operacion.IRead(true));
      if (
        // eslint-disable-next-line eqeqeq
        content[1].numero == this.datos.numero
        // eslint-disable-next-line eqeqeq
        && content[1].titulo == this.datos.titulo
        // eslint-disable-next-line eqeqeq
        && content[1].contenido == this.datos.contenido
      ) return true;
    }
    return false;
  }
}
