import ITransaction from './ITransaction';

export default class ReadData {
    datos: any

    operacion: ITransaction

    getSingle: boolean

    constructor(datos: object, getSingle: boolean) {
      this.datos = datos;
      this.operacion = new ITransaction(this.datos, false);
      this.getSingle = getSingle;
    }

    async run(): Promise<[number, object]> {
      // eslint-disable-next-line no-return-await
      return (await this.operacion.IRead(this.getSingle));
    }
}
