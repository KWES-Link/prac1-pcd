"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ITransaction_1 = __importDefault(require("./ITransaction"));
class ReadData {
    constructor(datos, getSingle) {
        this.datos = datos;
        this.operacion = new ITransaction_1.default(this.datos, false);
        this.getSingle = getSingle;
    }
    async run() {
        const OUTPUT = await this.operacion.IRead(this.getSingle);
        return OUTPUT;
    }
}
exports.default = ReadData;
//# sourceMappingURL=ReadData.js.map