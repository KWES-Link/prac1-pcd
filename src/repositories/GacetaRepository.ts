// @ts-ignore
import db from '../../models';

export default class GacetaRepository {
    datos: any

    constructor(datos: object) {
      this.datos = datos;
    }

    /**
   * Estando correcta la información (al llegar a este punto) se guarda la información en la base
   * de datos ejecutando una serie de validaciones adicionales las cuales en caso de que exista
   * la edición o el numero se hará caso omiso ya que la creación de estas se llevará acabo siempre
   * y cuando, estos no existan en la base de datos. Esto para evitar duplicados.
   */
    async setArticulo(): Promise<number> {
      const EDICION: any = await db.Editions.findOne({ where: { name: this.datos.edicion } });
      const IDEDICION: number = parseInt(EDICION ? EDICION.dataValues.id : '1', 10);
      if (!EDICION) await db.Editions.create({ name: this.datos.edicion });

      const NUMERO: any = await db.Numbers.findOne({
        where: {
          editionID: IDEDICION,
          number: this.datos.numero,
        },
      });
      const CONSECNUMERO: any = await db.Numbers.findAll({
        limit: 1,
        order: [['createdAt', 'DESC']],
      });
      const IDNUMERO: number = parseInt(CONSECNUMERO.length
        ? `${parseInt(CONSECNUMERO[0].dataValues.id, 10) + 1}`
        : '1', 10);
      if (!NUMERO) {
        await db.Numbers.create({
          editionID: IDEDICION,
          number: this.datos.numero,
          date: this.datos.fecha,
        });
      }

      await db.Articles.create({
        numberID: !NUMERO ? IDNUMERO : NUMERO.dataValues.id,
        title: this.datos.titulo,
        content: this.datos.contenido,
      });
      return 200;
    }

    /**
   * Funciona de forma similar a setArticulo solo que en caso de que se cambie la edición que ya
   * exista, solo se va a cambiar la ID de edición y la  edición anterior se va a eliminar.
   * Esto para evitar duplicados.
   */
    async setCambioArticulo(): Promise<number> {
      let edicion: any = await db.Editions.findOne({ where: { name: this.datos.edicionAnterior } });

      const NUMEROANTERIOR: any = await db.Numbers.findOne({
        where: {
          editionID: edicion.dataValues.id,
          number: this.datos.numeroAnterior,
        },
      });

      const ARTICULOANTERIOR: any = await db.Articles.findOne({
        where: {
          numberID: NUMEROANTERIOR.dataValues.id,
          title: this.datos.tituloAnterior,
        },
      });

      if (!(await db.Editions.findOne({ where: { name: this.datos.edicion } }))) {
        db.Editions.create({ name: this.datos.edicion });
        edicion = await db.Editions.findOne({ where: { name: this.datos.edicion } });
      }
      const IDEDICION: number = parseInt(edicion.dataValues.id, 10);

      let numero: any = await db.Numbers.findOne({
        where: {
          editionID: IDEDICION,
          number: this.datos.numero,
        },
      });

      if (this.datos.numero !== this.datos.numeroAnterior) {
        if (!numero) {
          await db.Numbers.update({
            editionID: IDEDICION,
            number: this.datos.numero,
            date: this.datos.fecha,
          }, {
            where: {
              id: NUMEROANTERIOR.dataValues.id,
            },
          });
          numero = await db.Numbers.findOne({
            where: {
              editionID: IDEDICION,
              number: this.datos.numero,
            },
          });
        } else {
          await db.Numbers.destroy({
            where: {
              editionID: IDEDICION,
              number: this.datos.numeroAnterior,
            },
          });
        }
      }

      await db.Articles.update({
        numberID: numero.dataValues.id,
        title: this.datos.titulo,
        content: this.datos.contenido,
      }, {
        where: {
          id: ARTICULOANTERIOR.dataValues.id,
        },
      });
      return 200;
    }

    /**
   * Realiza la busqueda del articulo, en caso de que se encuentre la información en cada tabla,
   * va a devolver la información, de lo contrario va a devolver error 404 con su respectivo mensaje
   */
    async getArticulo(): Promise<[number, object]> {
      let NUMERO = null;
      let ARTICULO = null;
      const EDICION: any = await db.Editions.findOne({
        where: {
          name: this.datos.edicion,
        },
      });
      if (EDICION) {
        NUMERO = await db.Numbers.findOne({
          where: {
            editionID: EDICION.dataValues.id,
            number: this.datos.numero,
          },
        });
        if (NUMERO) {
          ARTICULO = await db.Articles.findOne({
            where: {
              numberID: NUMERO.dataValues.id,
              title: this.datos.titulo,
            },
          });
          if (ARTICULO) {
            return [
              200,
              {
                fecha: NUMERO.dataValues.date,
                edicion: EDICION.dataValues.name,
                numero: NUMERO.dataValues.number,
                titulo: ARTICULO.dataValues.title,
                contenido: ARTICULO.dataValues.content,
              },
            ];
          }
        }
      }

      return [404, {
        status: 'Article or Number Not Found',
        message: `El artículo "${this.datos.titulo}" no existe en la edición ${this.datos.edicion}`
              + ` no. ${this.datos.numero}!`,
      }];
    }

    /**
   * Funciona de manera similar que getArticulo solo que por default se regresa 200 ya que la
   * información está en orden. En caso de que no haya resultados para EDICION o NUMERO va a
   * devolver error 404 con su mensaje respectivo
   */
    async getArticulos(): Promise<[number, object]> {
      const EDICION: any = await db.Editions.findOne({
        where: {
          name: this.datos.edicion,
        },
      });

      const NUMERO: any = await db.Numbers.findOne({
        where: {
          editionID: EDICION.id,
          number: this.datos.numero,
        },
      });

      const ARTICULO: any = await db.Articles.findAll({
        where: {
          numberID: NUMERO.dataValues.id,
        },
      });

      const TITULOS = ARTICULO.map((i) => i.dataValues.title);

      if (!NUMERO || !EDICION) {
        return [404, {
          status: 'Edition or Number Not Found',
          message: 'La edición o el articulo que estas buscando no existe, verifica la información '
              + 'e intentalo nuevamente!',
        }];
      }
      return [200, {
        fecha: NUMERO.dataValues.date,
        edicion: EDICION.dataValues.name,
        numero: NUMERO.dataValues.number,
        titulo: TITULOS,
      }];
    }
}
