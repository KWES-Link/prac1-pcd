"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// @ts-ignore
const models_1 = __importDefault(require("../../models"));
class GacetaRepository {
    constructor(datos) {
        this.datos = datos;
    }
    /**
   * Estando correcta la información (al llegar a este punto) se guarda la información en la base
   * de datos ejecutando una serie de validaciones adicionales las cuales en caso de que exista
   * la edición o el numero se hará caso omiso ya que la creación de estas se llevará acabo siempre
   * y cuando, estos no existan en la base de datos. Esto para evitar duplicados.
   */
    async setArticulo() {
        const EDICION = await models_1.default.Editions.findOne({ where: { name: this.datos.edicion } });
        const IDEDICION = parseInt(EDICION ? EDICION.dataValues.id : '1', 10);
        if (!EDICION)
            await models_1.default.Editions.create({ name: this.datos.edicion });
        const NUMERO = await models_1.default.Numbers.findOne({
            where: {
                editionID: IDEDICION,
                number: this.datos.numero,
            },
        });
        const CONSECNUMERO = await models_1.default.Numbers.findAll({
            limit: 1,
            order: [['createdAt', 'DESC']],
        });
        const IDNUMERO = parseInt(CONSECNUMERO.length
            ? `${parseInt(CONSECNUMERO[0].dataValues.id, 10) + 1}`
            : '1', 10);
        if (!NUMERO) {
            await models_1.default.Numbers.create({
                editionID: IDEDICION,
                number: this.datos.numero,
                date: this.datos.fecha,
            });
        }
        await models_1.default.Articles.create({
            numberID: !NUMERO ? IDNUMERO : NUMERO.dataValues.id,
            title: this.datos.titulo,
            content: this.datos.contenido,
        });
        return 200;
    }
    /**
   * Funciona de forma similar a setArticulo solo que en caso de que se cambie la edición que ya
   * exista, solo se va a cambiar la ID de edición y la  edición anterior se va a eliminar.
   * Esto para evitar duplicados.
   */
    async setCambioArticulo() {
        let edicion = await models_1.default.Editions.findOne({ where: { name: this.datos.edicionAnterior } });
        const NUMEROANTERIOR = await models_1.default.Numbers.findOne({
            where: {
                editionID: edicion.dataValues.id,
                number: this.datos.numeroAnterior,
            },
        });
        const ARTICULOANTERIOR = await models_1.default.Articles.findOne({
            where: {
                numberID: NUMEROANTERIOR.dataValues.id,
                title: this.datos.tituloAnterior,
            },
        });
        if (!(await models_1.default.Editions.findOne({ where: { name: this.datos.edicion } }))) {
            models_1.default.Editions.create({ name: this.datos.edicion });
            edicion = await models_1.default.Editions.findOne({ where: { name: this.datos.edicion } });
        }
        const IDEDICION = parseInt(edicion.dataValues.id, 10);
        let numero = await models_1.default.Numbers.findOne({
            where: {
                editionID: IDEDICION,
                number: this.datos.numero,
            },
        });
        if (this.datos.numero !== this.datos.numeroAnterior) {
            if (!numero) {
                await models_1.default.Numbers.update({
                    editionID: IDEDICION,
                    number: this.datos.numero,
                    date: this.datos.fecha,
                }, {
                    where: {
                        id: NUMEROANTERIOR.dataValues.id,
                    },
                });
                numero = await models_1.default.Numbers.findOne({
                    where: {
                        editionID: IDEDICION,
                        number: this.datos.numero,
                    },
                });
            }
            else {
                await models_1.default.Numbers.destroy({
                    where: {
                        editionID: IDEDICION,
                        number: this.datos.numeroAnterior,
                    },
                });
            }
        }
        await models_1.default.Articles.update({
            numberID: numero.dataValues.id,
            title: this.datos.titulo,
            content: this.datos.contenido,
        }, {
            where: {
                id: ARTICULOANTERIOR.dataValues.id,
            },
        });
        return 200;
    }
    /**
   * Realiza la busqueda del articulo, en caso de que se encuentre la información en cada tabla,
   * va a devolver la información, de lo contrario va a devolver error 404 con su respectivo mensaje
   */
    async getArticulo() {
        let NUMERO = null;
        let ARTICULO = null;
        const EDICION = await models_1.default.Editions.findOne({
            where: {
                name: this.datos.edicion,
            },
        });
        if (EDICION) {
            NUMERO = await models_1.default.Numbers.findOne({
                where: {
                    editionID: EDICION.dataValues.id,
                    number: this.datos.numero,
                },
            });
            if (NUMERO) {
                ARTICULO = await models_1.default.Articles.findOne({
                    where: {
                        numberID: NUMERO.dataValues.id,
                        title: this.datos.titulo,
                    },
                });
                if (ARTICULO) {
                    return [
                        200,
                        {
                            fecha: NUMERO.dataValues.date,
                            edicion: EDICION.dataValues.name,
                            numero: NUMERO.dataValues.number,
                            titulo: ARTICULO.dataValues.title,
                            contenido: ARTICULO.dataValues.content,
                        },
                    ];
                }
            }
        }
        return [404, {
                status: 'Article or Number Not Found',
                message: `El artículo "${this.datos.titulo}" no existe en la edición ${this.datos.edicion}`
                    + ` no. ${this.datos.numero}!`,
            }];
    }
    /**
   * Funciona de manera similar que getArticulo solo que por default se regresa 200 ya que la
   * información está en orden. En caso de que no haya resultados para EDICION o NUMERO va a
   * devolver error 404 con su mensaje respectivo
   */
    async getArticulos() {
        const EDICION = await models_1.default.Editions.findOne({
            where: {
                name: this.datos.edicion,
            },
        });
        const NUMERO = await models_1.default.Numbers.findOne({
            where: {
                editionID: EDICION.id,
                number: this.datos.numero,
            },
        });
        console.log(NUMERO);
        const ARTICULO = await models_1.default.Articles.findAll({
            where: {
                numberID: NUMERO.dataValues.id,
            },
        });
        console.log(ARTICULO);
        const TITULOS = ARTICULO.map((i) => i.dataValues.title);
        if (!NUMERO || !EDICION) {
            return [404, {
                    status: 'Edition or Number Not Found',
                    message: 'La edición o el articulo que estas buscando no existe, verifica la información '
                        + 'e intentalo nuevamente!',
                }];
        }
        return [200, {
                fecha: NUMERO.dataValues.date,
                edicion: EDICION.dataValues.name,
                numero: NUMERO.dataValues.number,
                titulo: TITULOS,
            }];
    }
}
exports.default = GacetaRepository;
//# sourceMappingURL=GacetaRepository.js.map